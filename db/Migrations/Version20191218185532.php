<?php

declare(strict_types=1);

namespace Database\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191218185532 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
		$this->addSql("INSERT INTO `typ_narodnost` (`nazev`, `created_at`, `updated_at`)
VALUES ('United Kingdom', now(), now());");
		$this->addSql("INSERT INTO `typ_narodnost` (`nazev`, `created_at`, `updated_at`)
VALUES ('United States of America', now(), now());");
		$this->addSql("INSERT INTO `typ_narodnost` (`nazev`, `created_at`, `updated_at`)
VALUES ('Czech Republic', now(), now());");

		$this->addSql("INSERT INTO `typ_zanr` (`nazev`, `created_at`, `updated_at`)
VALUES ('Rock', now(), now());");
		$this->addSql("INSERT INTO `typ_zanr` (`nazev`, `created_at`, `updated_at`)
VALUES ('Pop', now(), now());");
		$this->addSql("INSERT INTO `typ_zanr` (`nazev`, `created_at`, `updated_at`)
VALUES ('Classic', now(), now());");

		$this->addSql("INSERT INTO `interpret` (`id_typ_narodnost`, `nazev`, `created_at`, `updated_at`)
VALUES ('1', 'Muse', now(), now());");
		$this->addSql("INSERT INTO `interpret` (`id_typ_narodnost`, `nazev`, `created_at`, `updated_at`)
VALUES ('2', 'Justin Timberlake', now(), now());");
		$this->addSql("INSERT INTO `interpret` (`id_typ_narodnost`, `nazev`, `created_at`, `updated_at`)
VALUES ('3', 'Antonín Dvořák', now(), now());");

		$this->addSql("INSERT INTO `skladba` (`nazev`, `delka`, `created_at`, `updated_at`)
VALUES ('Dig Down', '00:03:48', now(), now());");
		$this->addSql("INSERT INTO `skladba` (`nazev`, `delka`, `created_at`, `updated_at`)
VALUES ('Can`t stop the feeling', '00:03:56', now(), now());");
		$this->addSql("INSERT INTO `skladba` (`nazev`, `delka`, `created_at`, `updated_at`)
VALUES ('From the New World Largo', '00:09:55', now(), now());");
		$this->addSql("INSERT INTO `skladba` (`nazev`, `delka`, `created_at`, `updated_at`)
VALUES ('Something Human', '00:03:46', now(), now());");

		$this->addSql("INSERT INTO `album` (`id_typ_zanr`, `nazev`, `created_at`, `updated_at`)
VALUES ('1', 'Simulation theory', now(), now());");
		$this->addSql("INSERT INTO `album` (`id_typ_zanr`, `nazev`, `created_at`, `updated_at`)
VALUES ('2', 'Can`t stop the feeling', now(), now());");
		$this->addSql("INSERT INTO `album` (`id_typ_zanr`, `nazev`, `created_at`, `updated_at`)
VALUES ('3', 'The best of Antonín Dvořák', now(), now());");

		$this->addSql("INSERT INTO `album_interpret` (`id_album`, `id_interpret`, `created_at`, `updated_at`)
VALUES ('1', '1', now(), now());");
		$this->addSql("INSERT INTO `album_interpret` (`id_album`, `id_interpret`, `created_at`, `updated_at`)
VALUES ('2', '2', now(), now());");
		$this->addSql("INSERT INTO `album_interpret` (`id_album`, `id_interpret`, `created_at`, `updated_at`)
VALUES ('3', '3', now(), now());");

		$this->addSql("INSERT INTO `album_skladba` (`id_album`, `id_skladba`, `cislo_stopy`, `created_at`, `updated_at`)
VALUES ('1', '1', '10', now(), now());");
		$this->addSql("INSERT INTO `album_skladba` (`id_album`, `id_skladba`, `cislo_stopy`, `created_at`, `updated_at`)
VALUES ('2', '2', '1', now(), now());");
		$this->addSql("INSERT INTO `album_skladba` (`id_album`, `id_skladba`, `cislo_stopy`, `created_at`, `updated_at`)
VALUES ('3', '3', '1', now(), now());");
		$this->addSql("INSERT INTO `album_skladba` (`id_album`, `id_skladba`, `cislo_stopy`, `created_at`, `updated_at`)
VALUES ('1', '4', '6', '', NULL);");


    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
