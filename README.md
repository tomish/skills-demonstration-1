## PLACHY TOMAS - SKILLS DEMONSTRATION

* gitlab :      https://gitlab.com/tomish/skills-demonstration-1
* linkedin :    https://www.linkedin.com/in/tomasplachy/

Welcome to my skills demonstration project. I have used a Nutella Project for build a skeleton (https://github.com/planette/nutella-project)

I primary use this technologies:
* Nette 3.0
* Nettrine ORM for creating and manipulating database structure
* Nettrine migrations for filling database

## REQUIREMENTS

check ``composer.json`` file, minimum requirements:
* PHP >= 7.3 (Best on LAMP)
* MYSQL (I am using Ver 8.0.18),
* thanks to Nettrine/Doctrine you can use other SQL server by changing of connection driver, more in settings ``app/config/app/parameters.neon:14``
* composer (https://getcomposer.org/)

### INSTALL
1) install all packages of vendor by composer, run in console:

```bash
composer install
mkdir temp
sudo chmod 0777 temp -r
mkdir log
sudo chmod 0777 log -r
```

2) set/create ``app/config/config.local.neon`` :

```neon
parameters:
	database:
		host: localhost
		dbname: dbname
		user: login
		password: password
```

3) create new empty database

4) fill database, in console (if is all other working correcly) run:

```bash
# clear temp directory
sudo rm -rf temp/*

# create structure by orm
php bin/console orm:schema-tool:update --force

# run migrations for fill data to database
php bin/console migrations:migrate

# clear temp directory
sudo rm -rf temp/*
```
in case of any failure (doctrine update or migrations fails), run sql queries from ``db/Force Imports/skills-demonstration-1 (1).sql``

5) Run and enjoy

---------------------------------------

## SQL queries

* Select all albums with interpret and count of songs in album by doctrine:
```sql
SELECT a0_.nazev AS nazev_0, i1_.nazev AS nazev_1, COUNT(a2_.id) AS sclr_2 FROM `album` a0_ INNER JOIN `album_interpret` a3_ ON (a3_.id_album = a0_.id) INNER JOIN `interpret` i1_ ON (a3_.id_interpret = i1_.id) INNER JOIN `album_skladba` a2_ ON (a2_.id_album = a0_.id) GROUP BY a0_.id ORDER BY nazev_1 ASC, nazev_0 ASC
```
* Select all albums with interpret and count of songs in album in pure sql:
```sql
SELECT a.nazev AS album_nazev, i.nazev AS interpret_nazev, COUNT(albskl.id) as count_of_skladba FROM album a INNER JOIN album_interpret ai on a.id = ai.id_album INNER JOIN interpret i on ai.id_interpret = i.id INNER JOIN album_skladba albskl on a.id = albskl.id_album GROUP BY a.id ORDER BY interpret_nazev ASC, album_nazev ASC
```

* select album with interpret with longest song on album in pure sql:
```sql
SELECT a.nazev AS album_nazev, i.nazev AS interpret_nazev, s.delka AS delka, s.nazev FROM album a INNER JOIN album_interpret ai on a.id = ai.id_album INNER JOIN interpret i on ai.id_interpret = i.id INNER JOIN album_skladba `as` on a.id = `as`.id_album INNER JOIN skladba s on `as`.id_skladba = s.id GROUP BY a.id ORDER BY MAX(s.delka) DESC LIMIT 1
```

### HTML Elements builded by own components

Preview:
![](html-elements.png)
