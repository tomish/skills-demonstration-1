<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Img;

interface ImgFactoryInterface
{

	/**
	 * @return Img
	 */
	public function create();
}
