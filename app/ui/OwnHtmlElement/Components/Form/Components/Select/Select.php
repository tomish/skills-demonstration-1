<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Form\Components\Select;

use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleTrait;
use App\UI\OwnHtmlElement\Attributes\NameAbleTrait;
use App\UI\OwnHtmlElement\Attributes\ValueAbleTrait;
use App\UI\OwnHtmlElement\OwnHtmlElementInterface;
use App\UI\OwnHtmlElement\OwnHtmlElementTrait;
use Latte\Engine;

class Select implements SelectInterface
{
	use OwnHtmlElementTrait;
	use HtmlContentAbleTrait;
	use NameAbleTrait;

	/**
	 * @var OptionFactoryInterface
	 */
	private $optionFactory;

	public function __construct(OptionFactoryInterface $optionFactory)
	{
		$this->optionFactory = $optionFactory;
	}

	/**
	 * @var Option[]
	 */
	private $options = [];

	/**
	 * @inheritDoc
	 */
	public function render()
	{
		$latte = new Engine();

		$parameters = [
			'element' => $this,
		];
		$latte->render(__DIR__ . '/' . (new \ReflectionClass($this))->getShortName() . '.latte', $parameters);
	}


	/**
	 * @inheritDoc
	 */
	public function getAllOptions(): array
	{
		return $this->options;
	}

	/**
	 * @inheritDoc
	 */
	public function addOption(Option $option): SelectInterface
	{
		if(!$this->isOptionInOptions($option)) {
			$this->options[] = $option;
		}
		return $this;
	}

	private function isOptionInOptions(Option $optionForInsert) : bool
	{
		$isInOptions = false;
		/** @var Option $option */
		foreach ($this->options as $option) {
			$isInOptions = $option->getValueAttribute() ===  $optionForInsert->getValueAttribute() ? true : $isInOptions;
		}
		return $isInOptions;
	}
	/**
	 * @inheritDoc
	 */
	public function removeAllOptions(): SelectInterface
	{
		$this->options = [];
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function addOptionValue(string $value,
								   string $label,
								   bool $disabled = false,
								   bool $selected = false
	): SelectInterface
	{
		/** @var Option $option */
		$option = $this->optionFactory->create();
		$option->setValueAttribute($value)
		->setHtmlContent($label)
			->setDisabledAttribute($disabled)
			->setSelectedAttribute($selected);

		$this->addOption($option);
		return $this;
	}
}
