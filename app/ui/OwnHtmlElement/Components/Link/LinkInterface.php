<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Link;

use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleInterface;
use App\UI\OwnHtmlElement\Attributes\NameAbleInterface;
use App\UI\OwnHtmlElement\Attributes\TargetAbleInterface;
use App\UI\OwnHtmlElement\OwnHtmlElementInterface;

interface LinkInterface extends OwnHtmlElementInterface, NameAbleInterface, HtmlContentAbleInterface, TargetAbleInterface
{
	public function getHrefAttribute() : string;

	public function setHrefAttribute(string $attribute) : LinkInterface;


}
