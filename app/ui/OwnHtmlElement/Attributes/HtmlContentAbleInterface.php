<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Attributes;

use App\UI\OwnHtmlElement\OwnHtmlElementInterface;

interface HtmlContentAbleInterface
{
	/**
	 * @return string
	 */
	public function getHtmlContent() : string;

	/**
	 * @param string $content
	 *
	 * @return OwnHtmlElementInterface
	 */
	public function setHtmlContent(string $content);
}
