<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Attributes;

use App\UI\OwnHtmlElement\Components\Div\DivInterface;

trait HtmlContentAbleTrait
{
	/** @var string  */
	private $htmlContent = "";

	/**
	 * @inheritDoc
	 */
	public function getHtmlContent(): string
	{
		return $this->htmlContent;

	}

	/**
	 * @inheritDoc
	 */
	public function setHtmlContent(string $content)
	{
		$this->htmlContent = $content;
		return $this;
	}
}
