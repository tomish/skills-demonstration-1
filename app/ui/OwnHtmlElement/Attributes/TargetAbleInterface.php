<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Attributes;

use App\UI\OwnHtmlElement\OwnHtmlElementInterface;

interface TargetAbleInterface
{
	/**
	 * @return string
	 */
	public function getTargetAttribute() : string;

	/**
	 * @param string $attribute
	 *
	 * @return OwnHtmlElementInterface
	 */
	public function setTargetAttribute(string $attribute);
}
