<?php

declare(strict_types = 1);

namespace App\Model\Database\Entity;

use App\Model\Database\Entity\Attributes\TCreatedAt;
use App\Model\Database\Entity\Attributes\TId;
use App\Model\Database\Entity\Attributes\TUpdatedAt;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Model\Database\Repository\AlbumSkladbaRepository")
 * @ORM\Table(name="`album_skladba`", uniqueConstraints={@ORM\UniqueConstraint(columns={"id_album", "id_skladba", "cislo_stopy"})})
 * @ORM\HasLifecycleCallbacks
 */
class AlbumSkladba extends AbstractEntity
{
	use TId;
	use TCreatedAt;
	use TUpdatedAt;

	/**
	 * @ORM\ManyToOne(targetEntity="Album", inversedBy="skladby")
	 * @ORM\JoinColumn(onDelete="CASCADE", referencedColumnName="id", name="id_album")
	 */
	private $album;

	/**
	 * @ORM\ManyToOne(targetEntity="Skladba", inversedBy="alba")
	 * @ORM\JoinColumn(onDelete="CASCADE", referencedColumnName="id", name="id_skladba")
	 */
	private $skladba;

	/**
	 * @ORM\Column(type="integer", nullable=false, name="cislo_stopy", options={"unsigned":true, "default":0})
	 */
	private $cisloStopy;

	/**
	 * @return mixed
	 */
	public function getAlbum()
	{
		return $this->album;
	}

	/**
	 * @param mixed $album
	 *
	 * @return AlbumSkladba
	 */
	public function setAlbum($album) : AlbumSkladba
	{
		$this->album = $album;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSkladba()
	{
		return $this->skladba;
	}

	/**
	 * @param mixed $skladba
	 *
	 * @return AlbumSkladba
	 */
	public function setSkladba($skladba) : AlbumSkladba
	{
		$this->skladba = $skladba;
		return $this;
	}



}
