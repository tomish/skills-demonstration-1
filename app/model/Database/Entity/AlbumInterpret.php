<?php

declare(strict_types = 1);

namespace App\Model\Database\Entity;

use App\Model\Database\Entity\Attributes\TCreatedAt;
use App\Model\Database\Entity\Attributes\TId;
use App\Model\Database\Entity\Attributes\TUpdatedAt;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Model\Database\Repository\AlbumInterpretRepository")
 * @ORM\Table(name="`album_interpret`", uniqueConstraints={@ORM\UniqueConstraint(columns={"id_album", "id_interpret"})})
 * @ORM\HasLifecycleCallbacks
 */
class AlbumInterpret extends AbstractEntity
{
	use TId;
	use TCreatedAt;
	use TUpdatedAt;

	/**
	 * @ORM\ManyToOne(targetEntity="Album", inversedBy="interpreti")
	 * @ORM\JoinColumn(onDelete="CASCADE", referencedColumnName="id", name="id_album")
	 */
	private $album;

	/**
	 * @ORM\ManyToOne(targetEntity="Interpret", inversedBy="alba")
	 * @ORM\JoinColumn(onDelete="CASCADE", referencedColumnName="id", name="id_interpret")
	 */
	private $interpret;


	/**
	 * @return mixed
	 */
	public function getAlbum()
	{
		return $this->album;
	}

	/**
	 * @param mixed $album
	 *
	 * @return Album
	 */
	public function setAlbum($album) : Album
	{
		$this->album = $album;
		return $this;
	}

	/**
	 * @return Interpret
	 */
	public function getInterpret()
	{
		return $this->interpret;
	}

	/**
	 * @param Interpret $interpret
	 *
	 * @return AlbumInterpret
	 */
	public function setInterpret(Interpret $interpret): AlbumInterpret
	{
		$this->interpret = $interpret;
		return $this;
	}



}
