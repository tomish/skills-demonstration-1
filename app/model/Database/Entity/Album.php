<?php

declare(strict_types=1);

namespace App\Model\Database\Entity;

use App\Model\Database\Entity\Attributes\TCreatedAt;
use App\Model\Database\Entity\Attributes\TId;
use App\Model\Database\Entity\Attributes\TUpdatedAt;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Database\Repository\AlbumRepository")
 * @ORM\Table(name="`album`")
 * @ORM\HasLifecycleCallbacks
 */
class Album extends AbstractEntity
{
	use TId;
	use TCreatedAt;
	use TUpdatedAt;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=255, nullable=FALSE, unique=false)
	 */
	private $nazev;

	/**
	 * @var TypZanr
	 * @ORM\ManyToOne(targetEntity="TypZanr")
	 * @ORM\JoinColumn(onDelete="CASCADE", referencedColumnName="id", name="id_typ_zanr")
	 */
	private $typZanr;

	/**
	 * @var Skladba[]
	 * @ORM\OneToMany(targetEntity="AlbumSkladba", mappedBy="album", fetch="LAZY")
	 */
	private $skladby;

	/**
	 * @var AlbumInterpret[]
	 * @ORM\OneToMany(targetEntity="AlbumInterpret", mappedBy="album", fetch="LAZY")
	 */
	private $interpreti;

	/**
	 * @return Interpret[]
	 */
	public function getInterpreti(): array
	{
		$interpreti = [];
		/** @var  AlbumInterpret $albumInterpret */
		foreach ($this->interpreti as $index => $albumInterpret) {
			$interpreti[] = $albumInterpret->getInterpret();
		}
		return $interpreti;
	}

	/**
	 * @param Interpret[] $interpreti
	 * @todo: setter neni hotovy
	 *
	 * @return Album
	 */
	public function setInterpreti(array $interpreti): Album
	{
		$this->interpreti = $interpreti;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNazev(): string
	{
		return $this->nazev;
	}

	/**
	 * @param string $nazev
	 *
	 * @return Album
	 */
	public function setNazev(string $nazev): Album
	{
		$this->nazev = $nazev;
		return $this;
	}

	/**
	 * @return Skladba[]
	 */
	public function getSkladby(): array
	{
		 $skladby = [];
		/** @var  AlbumSkladba $albumSkladba */
		foreach ($this->skladby as $index => $albumSkladba) {
			$skladby[] = $albumSkladba->getSkladba();
		}
		return $skladby;
	}

	/**
	 * @param Skladba[] $skladby
	 * @todo: setter neni hotovy
	 *
	 * @return Album
	 */
	public function setSkladby(array $skladby): Album
	{
		$this->skladby = $skladby;
		return $this;
	}

	/**
	 * @return TypZanr
	 */
	public function getTypZanr(): TypZanr
	{
		return $this->typZanr;
	}

	/**
	 * @param TypZanr $typZanr
	 *
	 * @return Album
	 */
	public function setTypZanr(TypZanr $typZanr): Album
	{
		$this->typZanr = $typZanr;
		return $this;
	}

}
