<?php

declare(strict_types=1);

namespace App\Model\Database\Entity;

use App\Model\Database\Entity\Attributes\TCreatedAt;
use App\Model\Database\Entity\Attributes\TId;
use App\Model\Database\Entity\Attributes\TUpdatedAt;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Database\Repository\InterpretRepository")
 * @ORM\Table(name="`interpret`")
 * @ORM\HasLifecycleCallbacks
 */
class Interpret extends AbstractEntity
{
	use TId;
	use TCreatedAt;
	use TUpdatedAt;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=255, nullable=FALSE, unique=false)
	 */
	private $nazev;

	/**
	 * @var TypNarodnost
	 * @ORM\ManyToOne(targetEntity="TypNarodnost")
	 * @ORM\JoinColumn(onDelete="CASCADE", referencedColumnName="id", name="id_typ_narodnost")
	 */
	private $narodnost;

	/**
	 * @var AlbumInterpret[]
	 * @ORM\OneToMany(targetEntity="AlbumInterpret", mappedBy="interpret", fetch="LAZY")
	 */
	private $alba;

	/**
	 * @return Album[]
	 */
	public function getAlba(): array
	{
		$alba = [];
		/** @var  AlbumInterpret $albumInterpret */
		foreach ($this->alba as $index => $albumInterpret) {
			$alba[] = $albumInterpret->getAlbum();
		}
		return $alba;
	}

	/**
	 * @param Album[] $alba
	 *
	 * @return Interpret
	 */
	public function setAlba(array $alba): Interpret
	{
		$this->alba = $alba;
		return $this;
	}

	/**
	 * @return TypNarodnost
	 */
	public function getNarodnost(): TypNarodnost
	{
		return $this->narodnost;
	}

	/**
	 * @param TypNarodnost $narodnost
	 *
	 * @return Interpret
	 */
	public function setNarodnost(TypNarodnost $narodnost): Interpret
	{
		$this->narodnost = $narodnost;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNazev(): string
	{
		return $this->nazev;
	}

	/**
	 * @param string $nazev
	 *
	 * @return Interpret
	 */
	public function setNazev(string $nazev): Interpret
	{
		$this->nazev = $nazev;
		return $this;
	}

}
