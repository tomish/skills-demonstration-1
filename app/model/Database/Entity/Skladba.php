<?php

declare(strict_types = 1);

namespace App\Model\Database\Entity;

use App\Model\Database\Entity\Attributes\TCreatedAt;
use App\Model\Database\Entity\Attributes\TId;
use App\Model\Database\Entity\Attributes\TUpdatedAt;
use App\Model\Exception\Logic\InvalidArgumentException;
use App\Model\Security\Identity;
use App\Model\Utils\DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Database\Repository\SkladbaRepository")
 * @ORM\Table(name="`skladba`")
 * @ORM\HasLifecycleCallbacks
 */
class Skladba extends AbstractEntity
{
	use TId;
	use TCreatedAt;
	use TUpdatedAt;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=255, nullable=FALSE, unique=false)
	 */
	private $nazev;

	/**
	 * @var string
	 * @ORM\Column(type="time", nullable=FALSE, unique=false)
	 */
	private  $delka;

	/**
	 * @var Album[]
	 * @ORM\OneToMany(targetEntity="AlbumSkladba", mappedBy="skladba", fetch="LAZY")
	 */
	private $alba;

	/**
	 * @return Album[]
	 */
	public function getAlba(): array
	{
		$alba = [];
		/** @var  AlbumSkladba $albumSkladba */
		foreach ($this->alba as $index => $albumSkladba) {
			$alba[] = $albumSkladba->getAlbum();
		}
		return $alba;
	}

	/**
	 * @param Album[] $alba
	 * @todo: setter neni hotovy
	 *
	 * @return Skladba
	 */
	public function setAlba(array $alba): Skladba
	{
		$this->alba = $alba;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNazev(): string
	{
		return $this->nazev;
	}

	/**
	 * @param string $nazev
	 *
	 * @return Skladba
	 */
	public function setNazev(string $nazev): Skladba
	{
		$this->nazev = $nazev;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDelka(): string
	{
		return $this->delka;
	}

	/**
	 * @param string $delka
	 *
	 * @return Skladba
	 */
	public function setDelka(string $delka): Skladba
	{
		$this->delka = $delka;
		return $this;
	}

}
