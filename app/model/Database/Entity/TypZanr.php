<?php

declare(strict_types = 1);

namespace App\Model\Database\Entity;

use App\Model\Database\Entity\Attributes\TCreatedAt;
use App\Model\Database\Entity\Attributes\TId;
use App\Model\Database\Entity\Attributes\TUpdatedAt;
use App\Model\Exception\Logic\InvalidArgumentException;
use App\Model\Security\Identity;
use App\Model\Utils\DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Database\Repository\TypZanrRepository")
 * @ORM\Table(name="`typ_zanr`")
 * @ORM\HasLifecycleCallbacks
 */
class TypZanr extends AbstractEntity
{
	use TId;
	use TCreatedAt;
	use TUpdatedAt;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=255, nullable=FALSE, unique=false)
	 */
	private $nazev;


	/**
	 * @return string
	 */
	public function getNazev(): string
	{
		return $this->nazev;
	}

	/**
	 * @param string $nazev
	 *
	 * @return TypZanr
	 */
	public function setNazev(string $nazev): TypZanr
	{
		$this->nazev = $nazev;
		return $this;
	}

}
