<?php declare(strict_types = 1);

namespace App\Model\Database;

use App\Model\Database\Entity\Album;
use App\Model\Database\Entity\AlbumInterpret;
use App\Model\Database\Entity\AlbumSkladba;
use App\Model\Database\Entity\Interpret;
use App\Model\Database\Entity\Skladba;
use App\Model\Database\Entity\TypNarodnost;
use App\Model\Database\Entity\TypZanr;
use App\Model\Database\Entity\User;
use App\Model\Database\Repository\AlbumInterpretRepository;
use App\Model\Database\Repository\AlbumRepository;
use App\Model\Database\Repository\AlbumSkladbaRepository;
use App\Model\Database\Repository\InterpretRepository;
use App\Model\Database\Repository\SkladbaRepository;
use App\Model\Database\Repository\TypNarodnostRepository;
use App\Model\Database\Repository\TypZanrRepository;
use App\Model\Database\Repository\UserRepository;

/**
 * @mixin EntityManager
 */
trait TRepositories
{

	public function getUserRepository(): UserRepository
	{
		return $this->getRepository(User::class);
	}

	public function getAlbumInterpretRepository() : AlbumInterpretRepository
	{
		return $this->getRepository(AlbumInterpret::class);
	}

	public function getAlbumRepository() : AlbumRepository
	{
		return $this->getRepository(Album::class);
	}

	public function getAlbumSkladbaRepository() : AlbumSkladbaRepository
	{
		return $this->getRepository(AlbumSkladba::class);
	}

	public function getInterpretRepository() : InterpretRepository
	{
		return $this->getRepository(Interpret::class);
	}

	public function getSkladbaRepository() : SkladbaRepository
	{
		return $this->getRepository(Skladba::class);
	}

	public function getTypNarodnostRepository() : TypNarodnostRepository
	{
		return $this->getRepository(TypNarodnost::class);
	}

	public function getTypZanrRepository() : TypZanrRepository
	{
		return $this->getRepository(TypZanr::class);
	}

}
