<?php declare(strict_types = 1);

namespace App\Model\Database\Repository;

use App\Model\Database\Entity\AlbumSkladba;

/**
 * @method AlbumSkladba|NULL find($id, ?int $lockMode = NULL, ?int $lockVersion = NULL)
 * @method AlbumSkladba|NULL findOneBy(array $criteria, array $orderBy = NULL)
 * @method AlbumSkladba[] findAll()
 * @method AlbumSkladba[] findBy(array $criteria, array $orderBy = NULL, ?int $limit = NULL, ?int $offset = NULL)
 * @extends AbstractRepository<User>
 */
class AlbumSkladbaRepository extends AbstractRepository
{

	public function findOneByNazev(string  $nazev): ?AlbumSkladba
	{
		return $this->findOneBy(['nazev' => $nazev]);
	}

}
