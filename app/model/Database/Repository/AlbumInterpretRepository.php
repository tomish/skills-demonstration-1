<?php declare(strict_types = 1);

namespace App\Model\Database\Repository;

use App\Model\Database\Entity\AlbumInterpret;

/**
 * @method AlbumInterpret|NULL find($id, ?int $lockMode = NULL, ?int $lockVersion = NULL)
 * @method AlbumInterpret|NULL findOneBy(array $criteria, array $orderBy = NULL)
 * @method AlbumInterpret[] findAll()
 * @method AlbumInterpret[] findBy(array $criteria, array $orderBy = NULL, ?int $limit = NULL, ?int $offset = NULL)
 * @extends AbstractRepository<User>
 */
class AlbumInterpretRepository extends AbstractRepository
{

	public function findOneByNazev(string  $nazev): ?AlbumInterpret
	{
		return $this->findOneBy(['nazev' => $nazev]);
	}

}
