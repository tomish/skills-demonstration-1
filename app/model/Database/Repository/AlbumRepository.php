<?php declare(strict_types=1);

namespace App\Model\Database\Repository;

use App\Model\Database\Entity\Album;
use App\Model\Database\Entity\AlbumInterpret;
use App\Model\Database\Entity\AlbumSkladba;
use App\Model\Database\Entity\Interpret;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Album|NULL find($id, ?int $lockMode = null, ?int $lockVersion = null)
 * @method Album|NULL findOneBy(array $criteria, array $orderBy = null)
 * @method Album[] findAll()
 * @method Album[] findBy(array $criteria, array $orderBy = null, ?int $limit = null, ?int $offset = null)
 * @extends AbstractRepository<User>
 */
class AlbumRepository extends AbstractRepository
{

	public function findAlbumWithLongestTrackBySql() : ?array
	{
		$sql = "
		 SELECT a.nazev AS album_nazev, i.nazev AS interpret_nazev, s.delka AS delka, s.nazev
		 FROM album a
		 INNER JOIN album_interpret ai on a.id = ai.id_album
		 INNER JOIN interpret i on ai.id_interpret = i.id
		 INNER JOIN album_skladba `as` on a.id = `as`.id_album
		 INNER JOIN skladba s on `as`.id_skladba = s.id
		 GROUP BY a.id
		 ORDER BY MAX(s.delka) DESC LIMIT 1
		";

		$result = $this->_em->getConnection()->prepare($sql);
		$result->execute();
		return $result->fetch();
	}

	public function findAllByDQL()
	{
		/** @var QueryBuilder $qb */
		$qb = $this->_em->createQueryBuilder();

		$qb->select('a.nazev AS album_nazev, i.nazev AS interpret_nazev, COUNT(albskl.id) AS count_of_skladba')->from(Album::class,
			'a')
			->innerJoin(AlbumInterpret::class, 'ai', 'WITH', 'ai.album = a')
			->innerJoin(Interpret::class, 'i', 'WITH', 'ai.interpret = i')
			->innerJoin(AlbumSkladba::class, 'albskl', 'WITH', 'albskl.album = a')
			->addGroupBy('a.id')
			->addOrderBy('interpret_nazev', 'asc')
			->addOrderBy('album_nazev', 'asc');

		return $qb->getQuery()->getResult();
	}

	public function findAllByOrm()
	{
		/** @var QueryBuilder $qb */
		$qb = $this->_em->createQueryBuilder();
		$qb->select('a')->from(Album::class, 'a');
		return $qb->getQuery()->getResult();
	}

	public function findAllBySQL()
	{
		$sql = "
			SELECT a.nazev AS album_nazev, i.nazev AS interpret_nazev, COUNT(albskl.id) as count_of_skladba
			FROM album a
			INNER JOIN album_interpret ai on a.id = ai.id_album
			INNER JOIN interpret i on ai.id_interpret = i.id
			INNER JOIN album_skladba albskl on a.id = albskl.id_album
			GROUP BY a.id
			ORDER BY interpret_nazev ASC, album_nazev ASC

		";
		$result = $this->_em->getConnection()->prepare($sql);
		$result->execute();
		return $result->fetchAll();
	}

	public function findOneByNazev(string $nazev): ?Album
	{
		return $this->findOneBy(['nazev' => $nazev]);
	}

}
