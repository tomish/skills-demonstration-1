<?php declare(strict_types = 1);

namespace App\Model\Database\Repository;

use App\Model\Database\Entity\TypNarodnost;

/**
 * @method TypNarodnost|NULL find($id, ?int $lockMode = NULL, ?int $lockVersion = NULL)
 * @method TypNarodnost|NULL findOneBy(array $criteria, array $orderBy = NULL)
 * @method TypNarodnost[] findAll()
 * @method TypNarodnost[] findBy(array $criteria, array $orderBy = NULL, ?int $limit = NULL, ?int $offset = NULL)
 * @extends AbstractRepository<User>
 */
class TypNarodnostRepository extends AbstractRepository
{

	public function findOneByNazev(string  $nazev): ?TypNarodnost
	{
		return $this->findOneBy(['nazev' => $nazev]);
	}

}
