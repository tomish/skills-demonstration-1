<?php declare(strict_types = 1);

namespace App\Model\Database\Repository;

use App\Model\Database\Entity\Skladba;
use App\Model\Database\Entity\User;

/**
 * @method Skladba|NULL find($id, ?int $lockMode = NULL, ?int $lockVersion = NULL)
 * @method Skladba|NULL findOneBy(array $criteria, array $orderBy = NULL)
 * @method Skladba[] findAll()
 * @method Skladba[] findBy(array $criteria, array $orderBy = NULL, ?int $limit = NULL, ?int $offset = NULL)
 * @extends AbstractRepository<User>
 */
class SkladbaRepository extends AbstractRepository
{

	public function findOneByNazev(string  $nazev): ?Skladba
	{
		return $this->findOneBy(['nazev' => $nazev]);
	}

}
