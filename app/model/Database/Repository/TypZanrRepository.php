<?php declare(strict_types = 1);

namespace App\Model\Database\Repository;

use App\Model\Database\Entity\TypZanr;
/**
 * @method TypZanr|NULL find($id, ?int $lockMode = NULL, ?int $lockVersion = NULL)
 * @method TypZanr|NULL findOneBy(array $criteria, array $orderBy = NULL)
 * @method TypZanr[] findAll()
 * @method TypZanr[] findBy(array $criteria, array $orderBy = NULL, ?int $limit = NULL, ?int $offset = NULL)
 * @extends AbstractRepository<User>
 */
class TypZanrRepository extends AbstractRepository
{

	public function findOneByNazev(string  $nazev): ?TypZanr
	{
		return $this->findOneBy(['nazev' => $nazev]);
	}

}
