<?php declare(strict_types = 1);

namespace App\Model\Database\Repository;

use App\Model\Database\Entity\Interpret;

/**
 * @method Interpret|NULL find($id, ?int $lockMode = NULL, ?int $lockVersion = NULL)
 * @method Interpret|NULL findOneBy(array $criteria, array $orderBy = NULL)
 * @method Interpret[] findAll()
 * @method Interpret[] findBy(array $criteria, array $orderBy = NULL, ?int $limit = NULL, ?int $offset = NULL)
 * @extends AbstractRepository<User>
 */
class InterpretRepository extends AbstractRepository
{

	public function findOneByNazev(string  $nazev): ?Interpret
	{
		return $this->findOneBy(['nazev' => $nazev]);
	}

}
