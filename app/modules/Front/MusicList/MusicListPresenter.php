<?php declare(strict_types=1);

namespace App\Modules\Front\MusicList;

use App\Model\Database\Entity\Album;
use App\Model\Database\EntityManager;
use App\Modules\Front\BaseFrontPresenter;

final class MusicListPresenter extends BaseFrontPresenter
{

	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * @var Album[]|null
	 */
	private $albums;

	/**
	 * @var array
	 */
	private $statisticsByDQL = [];

	/**
	 * @var array
	 */
	private $statisticsBySQL = [];

	/**
	 * @var array
	 */
	private $longestTrackAlbum = [];

	public function __construct(EntityManager $em)
	{
		parent::__construct();
		$this->em = $em;
	}

	public function actionDefault()
	{
		$this->albums = $this->em->getAlbumRepository()->findAllByOrm();
		$this->statisticsByDQL = $this->em->getAlbumRepository()->findAllByDQL();
		$this->statisticsBySQL = $this->em->getAlbumRepository()->findAllBySQL();
		$this->longestTrackAlbum = $this->em->getAlbumRepository()->findAlbumWithLongestTrackBySql();

	}

	public function renderDefault()
	{
		$this->template->longestTrackAlbum = $this->longestTrackAlbum;
		$this->template->albums = $this->albums;
		$this->template->statisticsByDQL = $this->statisticsByDQL;
		$this->template->statisticsBySQL = $this->statisticsBySQL;
	}
}
