<?php declare(strict_types=1);

namespace App\Modules\Front\OwnHtmlElements;

use App\Model\Database\EntityManager;
use App\Modules\Front\BaseFrontPresenter;
use App\UI\OwnHtmlElement\Components\Div\Div;
use App\UI\OwnHtmlElement\Components\Div\DivFactoryInterface;
use App\UI\OwnHtmlElement\Components\Form\Components\Input\Input;
use App\UI\OwnHtmlElement\Components\Form\Components\Input\InputFactoryInterface;
use App\UI\OwnHtmlElement\Components\Form\Components\Select\SelectFactoryInterface;
use App\UI\OwnHtmlElement\Components\Form\Form;
use App\UI\OwnHtmlElement\Components\Form\FormFactoryInterface;
use App\UI\OwnHtmlElement\Components\Img\Img;
use App\UI\OwnHtmlElement\Components\Img\ImgFactoryInterface;
use App\UI\OwnHtmlElement\Components\Link\Link;
use App\UI\OwnHtmlElement\Components\Link\LinkFactoryInterface;
use App\UI\OwnHtmlElement\OwnHtmlElementInterface;

final class OwnHtmlElementsPresenter extends BaseFrontPresenter
{

	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * @var DivFactoryInterface
	 */
	private $divFactory;

	/** @var OwnHtmlElementInterface */
	private $rootDiv;

	/**
	 * @var ImgFactoryInterface
	 */
	private $imgFactory;

	/**
	 * @var FormFactoryInterface
	 */
	private $formFactory;

	/**
	 * @var InputFactoryInterface
	 */
	private $inputFactory;

	/**
	 * @var SelectFactoryInterface
	 */
	private $selectFactory;

	/**
	 * @var LinkFactoryInterface
	 */
	private $linkFactory;

	public function __construct(EntityManager $em,
								DivFactoryInterface $divFactory,
								ImgFactoryInterface $imgFactory,
								FormFactoryInterface $formFactory,
								InputFactoryInterface $inputFactory,
								SelectFactoryInterface $selectFactory,
								LinkFactoryInterface $linkFactory
	)
	{
		parent::__construct();
		$this->em = $em;
		$this->divFactory = $divFactory;
		$this->imgFactory = $imgFactory;
		$this->formFactory = $formFactory;
		$this->inputFactory = $inputFactory;
		$this->selectFactory = $selectFactory;
		$this->linkFactory = $linkFactory;
	}

	/**
	 * Better solution is in Nette/Forms. Its possible to use Checkbox and Radiobuttons lists
	 * @return OwnHtmlElementInterface
	 * @throws \Nette\Application\UI\InvalidLinkException
	 */
	private function fillForm(): OwnHtmlElementInterface
	{
		// form
		/** @var Form $form */
		$form = $this->formFactory->create();
		$form->setActionAttribute($this->link('doForm!'))
			->setMethodAttribute('post');

		// form input
		/** @var Input $formInputText */
		$formInputText = $this->inputFactory->create();

		$formItems = [];
		$formInputText->setTypeAttribute('text')
			->setNameAttribute('test-text-type')
			->setIdAttribute('form-test-text-type')
			->setClassAttribute('form-control')
			->setHtmlContent('Input type text');
		$formItems[] = $formInputText;

		/** @var Input $formInputRadio */
		$formInputRadio = $this->inputFactory->create();
		$formInputRadio->setTypeAttribute('radio')
			->setNameAttribute('test-radio-type')
			->setIdAttribute('form-test-radio-type-1')
			->setClassAttribute('form-check-input')
			->setHtmlContent('Select me')
			->setValueAttribute('option1');

		$formItems[] = $formInputRadio;

		/** @var Input $formInputRadio */
		$formInputRadio = $this->inputFactory->create();
		$formInputRadio->setTypeAttribute('radio')
			->setNameAttribute('test-radio-type')
			->setIdAttribute('form-test-radio-type-2')
			->setClassAttribute('form-check-input')
			->setHtmlContent('No! Select me please')
			->setValueAttribute('option2');

		$formItems[] = $formInputRadio;

		/** @var Input $formInputCheckbox */
		$formInputCheckbox = $this->inputFactory->create();
		$formInputCheckbox->setTypeAttribute('checkbox')
			->setNameAttribute('test-checkbox-type')
			->setIdAttribute('form-test-checkbox-type-1')
			->setClassAttribute('form-check-input')
			->setHtmlContent('Check me')
			->setValueAttribute('option1');

		$formItems[] = $formInputCheckbox;

		/** @var Input $formInputCheckbox */
		$formInputCheckbox = $this->inputFactory->create();
		$formInputCheckbox->setTypeAttribute('checkbox')
			->setNameAttribute('test-checkbox-type')
			->setIdAttribute('form-test-checkbox-type-2')
			->setClassAttribute('form-check-input')
			->setHtmlContent('Check me too')
			->setValueAttribute('option2');

		$formItems[] = $formInputCheckbox;

		$formSelect = $this->selectFactory->create();
		$formSelect->setNameAttribute('form-test-select-type')
			->setNameAttribute('test-select-type')
			->setClassAttribute('form-control')
			->addOptionValue('pop', 'POP music')
			->addOptionValue('rock', 'ROCK music - selected value', false, true)
			->addOptionValue('classic', 'Classic music - disabled value', true);

		$formItems[] = $formSelect;

		/** @var Input $formInputSubmit */
		$formInputSubmit = $this->inputFactory->create();
		$formInputSubmit->setTypeAttribute('submit')
			->setClassAttribute('btn btn-primary')
			->setValueAttribute('Odeslat formulář');

		foreach ($formItems as $formItem) {
			$form->addChildrenElement($formItem);
		}

		$form->addChildrenElement($formInputSubmit);

		return $form;
	}

	/**
	 * Better solution is move to some service
	 * @throws \Nette\Application\UI\InvalidLinkException
	 */
	public function actionDefault()
	{
		/** @var Div $div */
		$rootDiv = $this->divFactory->create();
		$rootDiv->setIdAttribute("root-own-html-element")
			->setClassAttribute('row');

		/** @var Div $childrenDiv */
		$childrenDiv = $this->divFactory->create();
		$childrenDiv->setIdAttribute('children-own-html-element')
			->setClassAttribute('col-xs-12 col-sm-12 col-md-12 col-lg-12')
			->setHtmlContent('<strong>pure html content by setter method in generated div element</strong><br><small>by  <code>\App\UI\OwnHtmlElement\Components\Div\Div</code></small><br>');

		$rootDiv->addChildrenElement($childrenDiv);

		/** @var Div $childrenDiv */
		$linkParentDiv = $this->divFactory->create();
		$linkParentDiv->setIdAttribute('link-parent-own-html-element')
			->setClassAttribute('col-xs-12 col-sm-12 col-md-12 col-lg-12')
			->setHtmlContent('<strong>link element generated</strong><br><small>by  <code>\App\UI\OwnHtmlElement\Components\Link\Link</code></small><br>');

		/** @var Link $link */
		$link = $this->linkFactory->create();
		$link->setHrefAttribute('http://plachytomas.cz')
			->setHtmlContent('<strong>plachy</strong>tomas.cz')
			->setTargetAttribute('_blank');

		$linkParentDiv->addChildrenElement($link);
		$rootDiv->addChildrenElement($linkParentDiv);

		for ($i = 0; $i < 3; $i++) {
			/** @var Div $childrenDiv */
			$imgParentDiv = $this->divFactory->create();
			$imgParentDiv->setIdAttribute('children-own-html-element')
				->setClassAttribute('col-xs-12 col-md-4')
				->setHtmlContent('<strong>Image element</strong><br><small>by  <code>\App\UI\OwnHtmlElement\Components\Img\Img</code></small><br>');

			/** @var Img $img */
			$img = $this->imgFactory->create();
			$img->setIdAttribute('some-img-id')
				->setClassAttribute('img-responsive')
				->setSrcAttribute('http://lorempixel.com/output/people-q-c-1920-1920-9.jpg')
				->setWidthAttribute(200)
				->setHeightAttribute(200)
				->setTitleAttribute('Titulek obrázku')
				->setAltAttribute('Alternativní název obrázku');

			$imgParentDiv->addChildrenElement($img);
			$rootDiv->addChildrenElement($imgParentDiv);
		}

		$formParentDiv = $this->divFactory->create();
		$formParentDiv->setIdAttribute('form-parent-own-html-element')
			->setClassAttribute('col-xs-12 col-sm-12 col-md-12 col-lg-12')
			->setHtmlContent('<strong>Form element</strong><br><small>by  <code>\App\UI\OwnHtmlElement\Components\Form\Form</small><br>');

		$formParentDiv->addChildrenElement($this->fillForm());
		$rootDiv->addChildrenElement($formParentDiv);

		$this->rootDiv = $rootDiv;
	}

	/**
	 * This is only demonstration, better usage in nette is via Nette/Forms objects
	 */
	public function handleDoForm()
	{
		$request = $this->getHttpRequest();
		$postValues = $request->getPost();
		\Tracy\Debugger::barDump($postValues, "this is only a raw demonstrations of getting data from form");
	}

	public function renderDefault()
	{
		$this->template->rootDiv = $this->rootDiv;
	}
}
