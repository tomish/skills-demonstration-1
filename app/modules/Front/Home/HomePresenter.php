<?php declare(strict_types=1);

namespace App\Modules\Front\Home;

use App\Model\Database\Entity\Album;
use App\Model\Database\EntityManager;
use App\Modules\Front\BaseFrontPresenter;

final class HomePresenter extends BaseFrontPresenter
{

	/**
	 * @var EntityManager
	 */
	private $em;


	public function __construct(EntityManager $em)
	{
		parent::__construct();
		$this->em = $em;
	}


}
